/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import SplashScreen from 'react-native-splash-screen';
import AppStack from './src/layouts/stacknav';

class App extends Component {
    constructor(props){
        super(props)       

    }

   async componentDidMount() {
    SplashScreen.hide();
   }

    render() { 
      return <AppStack/>
        }
         
    
}
 
export default App;
