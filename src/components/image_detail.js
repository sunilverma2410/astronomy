
import React, {PureComponent} from 'react';
import {
    Text,
    View,
    Image,
    ImageBackground,    
    Dimensions,  
    StatusBar,    
} from 'react-native';
import {Root} from 'native-base';
import {style} from '../layouts/style';
import Moment from 'moment';
export default class ImageDetail extends PureComponent {
    constructor(props) {
        super(props);       
        this.state = {           
            mainLoader:false, 
            imgData:{}                       
        };        
        this.imgPath='';        

    }


static navigationOptions = ({ navigation}) => {
  return{
    title: navigation.state.params.item.title,
    headerStyle: { backgroundColor: 'transparent',zIndex:100},
    headerTransparent:true,
    headerTintColor:'#fff'
  }
}
     
  componentWillMount(){
    let imgData=this.props.navigation.state.params.item;     
    this.setState({imgData:imgData});

  }
   

    render() {
        const {imgData} = this.state;
        
        return (           
               <Root>
 <StatusBar
  backgroundColor={'transparent'}
  barStyle="dark-content"  
/>
          <ImageBackground
                               style={{
                                flex:1,
                                width:null,
                                height:null,
                                justifyContent:'center',
                                alignContent:'center',      
                               
                              }}
                                source={imgData.source}                                
                            >   
                             <View style={style.overlay}></View>  
                             <View style={{width:'90%',alignSelf:'center', position:'absolute',bottom:2}}>
                             <Text style={{color:'#fff',fontSize:16,textAlign:'right',fontWeight:'400',marginRight:5}}>{Moment(imgData.date).format('MMM Do')}</Text>                                       
                             <Text style={{color:'#fff',fontSize:16,textAlign:'left',fontWeight:'400'}}>{imgData.title}</Text>                                     
                             <Text style={{color:'#fff',fontSize:12,textAlign:'justify'}}>{imgData.desc}</Text>          
                               </View>                              
                             </ImageBackground>
                
              
           </Root>
        );
    }
}

