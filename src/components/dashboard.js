
import React, {PureComponent} from 'react';
import {
    Text,
    View,    
    ImageBackground,
    TouchableOpacity,    
    Dimensions,   
    FlatList,
    StatusBar,
} from 'react-native';
import {Spinner,Card,CardItem} from 'native-base';
import {style, appColor, appColorLight} from '../layouts/style';
import IonIcon from "react-native-vector-icons/Ionicons";
import Icon from 'react-native-vector-icons/FontAwesome'
import Loader from '../layouts/loader';
import Moment from 'moment';

export default class Dashboard extends PureComponent {
    constructor(props) {
        super(props);       
        this.state = {
            images:[],
            lastEndDate:{}, 
            loadMore:false,            
            mainLoader:false,                          
        };
    }


    getFilterDate=(dt,flag='')=>{     
      if(flag==''){
        let today=new Date();
       let  endDateCalc=today.setMinutes(dt.getMinutes()-12960);
        let endDt=new Date(endDateCalc);
        let endDate=this.formatDate(endDt);
        let dts={};    
        dts.start_date=endDate;
        dts.end_date=this.formatDate(dt);
        return dts;  
      }else{
        let strtDateNew=new Date(dt);
        let strtDateOld=strtDateNew.setMinutes(dt.getMinutes()-1440);
        
        let strtDate=new Date(strtDateOld);  
        let strDt=new Date(strtDateOld);    
        let endDateCalc=strDt.setMinutes(dt.getMinutes()-12960);
        let endDt=new Date(endDateCalc);
        let endDate=this.formatDate(endDt);
        let dts={};    
        dts.start_date=endDate;
        dts.end_date=this.formatDate(strtDate);
        return dts;  
      }
      
     
     
   }
   formatDate =(date)=> {
     if(date){             
                  let month = date.getMonth() + 1;
                  let days = date.getDate();
                  month = month < 10 ? '0' + month : month;
                  days = days < 10 ? '0' + days : days;
                     return  date.getFullYear() + "-" + month + "-" + days;
                 
                }
              };
              

static navigationOptions = ({ navigation}) => {
  return{
    title: 'APOD',
    headerStyle: { backgroundColor: '#fff',zIndex:100},
    headerTransparent:true,
  

  }
}
      


    componentDidMount(){
        let params=this.getFilterDate(new Date());
        this.setState({lastEndDate:params.start_date,mainLoader:true});  
        // console.log("strt "+JSON.stringify(params))      
        this.fireAjax(apiUrl+'?api_key=ytqGOkcrNJPf3A5E51lFxXCNGUW160WYapO9CgP6&start_date='+encodeURIComponent(params.start_date)+'&end_date='+encodeURIComponent(params.end_date))
        .then((img)=>{
          // console.log("img"+JSON.stringify(img));
          this.setState({mainLoader:false})
          let imgData=[];
          let imgLen=img.length;          
          for(let i=imgLen-1;i>=0;i--){
            imgData.push({            
                source: {
                    uri:img[i].url,
                },
                title: img[i].title,              
                desc:img[i].explanation, 
                date:img[i].date             
  
            });
           
          }
          // console.log("imgdt"+JSON.stringify(img));
          this.setState({images:imgData});
        
        }).catch((err)=>{
          console.log("error");
        })
       
        
  
    }

    fireAjax=(url='') =>{
      if(url){                
        return fetch(url,{
          method: 'GET',
          headers: {         
            'Content-Type': 'application/x-www-form-urlencoded',
          }          
        })
        .then((response) => response.json())
        .then((responseJson) => {
         
          return responseJson;
        })
        .catch((error) => {
          console.log("error"+error);
        });
      }else{
        return false;
      }
     
    } 
    


    showMoreImage=async ()=>{       
        let params=this.getFilterDate(new Date(this.state.lastEndDate),1);
        this.setState({lastEndDate:params.start_date,loadMore:true});  
        let img= await this.fireAjax(apiUrl+'?api_key=DEMO_KEY&start_date='+encodeURIComponent(params.start_date)+'&end_date='+encodeURIComponent(params.end_date))
        if(img){
            let newImgData=[];
            let imgLen=img.length;
                if(imgLen>0){
           
            for(let i=imgLen-1;i>=0;i--){
                newImgData.push({            
                  source: {
                      uri:img[i].url,
                  },
                  title: img[i].title,              
                  desc:img[i].explanation, 
                  date:img[i].date,                 
    
              });
             
            }
        }else{
            this.setState({loadMore:false});
        }   
            
            let oldImages=Object.assign([],this.state.images);
            let combineRes=[...oldImages,...newImgData];         
          this.setState({
            loadMore:false,
            images:combineRes})                                 
        }else{
          this.setState({loadMore:false})
        }
      }


    render() {
        const {images,mainLoader,loadMore} = this.state;
        
        return (           
               <View style={{backgroundColor:'#fff',marginTop:50}}>
                <StatusBar
  backgroundColor={'transparent'}
  barStyle="dark-content"
  
/>
                 <Loader showLoader={mainLoader}/>
                <FlatList
           numColumns={2}
          data={images}
          renderItem={({item, index}) => (                 
                        <TouchableOpacity 
                        style={{width:'48%',margin:5}}                     
                            key={index}
                            onPress={() => this.props.navigation.navigate("ImageDetail",{item})}
                        >
                        <Card>
                       <CardItem cardBody>  
                                       
                       <ImageBackground
                               style={{height: 230, width:'100%', flex: 1}}
                                source={item.source}
                                resizeMode="cover"
                            >   
                             <View style={style.overlay}></View>  
                             <Text style={{color:'#fff',fontSize:16,textAlign:'right',fontWeight:'400',marginRight:5}}>{Moment(item.date).format('MMM Do')}</Text>          
                             <View style={{width:'90%',alignSelf:'center', alignContent:'center',alignItems:'center',position:'absolute',bottom:2}}>
                             <Text style={{color:'#fff',fontSize:16,textAlign:'center',fontWeight:'400'}}>{item.title}</Text>          
                               </View>                              
                             </ImageBackground>
                        </CardItem>
                       
                        </Card>
                           
                        </TouchableOpacity>
                    )}
                    keyExtractor={(i,index) => index}
                    // refreshing={isRefreshing}
                    // onRefresh={this.handleRefresh}     
                    ListFooterComponent={
                    <View style={{width:'100%',marginHorizontal:10,alignContent:'center',marginBottom:10,justifyContent:'center',alignItems:'center'}}>
                       {this.state.loadMore && <Spinner color={appColor} /> }
                       {images.length>0 && loadMore==false && 
                     <TouchableOpacity style={style.btnPrimary} onPress={this.showMoreImage}>         
                      <Text style={style.textWhite}>Show More  <IonIcon           
                        name="md-refresh"
                        size={20}
                        color={"#fff"}
                      /> 
                     </Text>
                    </TouchableOpacity>}
                    </View>  
                    } 
                    />
                
                
              
           </View>
        );
    }
}

const apiUrl='https://api.nasa.gov/planetary/apod';