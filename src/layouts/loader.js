import React, { Component } from 'react';
import {View,ActivityIndicator,Modal} from 'react-native';
class Loader extends Component {
    constructor(props){
      super(props)
     
    }
    render() { 
      const {showLoader}=this.props;
        return (

          <Modal onRequestClose={() => {
            //Alert.alert('Modal has been closed.');
          }}
           transparent={true} visible={showLoader} animationType='none' >
           <View style={{zIndex:150,flex:1,backgroundColor:'rgba(0,0,0,0.7)',justifyContent:'center',alignItems:'center'}}>
          <ActivityIndicator/>
          </View>
          </Modal>
    
          );
    }
}
 
export default Loader;
 