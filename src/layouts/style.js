import {StyleSheet,Dimensions} from 'react-native';
export const appColor="#008093";
export const style = StyleSheet.create({    
    overlay:{
      backgroundColor:'rgba(0,0,0,0.5)',
      ...StyleSheet.absoluteFillObject
    },
   
    textWhite:{
color:'#fff',
fontWeight:'400',
fontSize:16,
textAlign:'center'
    },
    btnPrimary:{
      backgroundColor:appColor,
      paddingHorizontal:15,
      paddingVertical:10,  
      borderRadius:2,               
      alignItems:'center',
      justifyContent:'center'
    },    
    
  });