import React, { Component } from "react";
import { 
  createStackNavigator, 
} from "react-navigation";
import Dashboard from "../components/dashboard";
import ImageDetail from "../components/image_detail";

const AppStack = createStackNavigator({
  Dashboard: {
    screen: Dashboard,   
  },
  ImageDetail: {
    screen: ImageDetail
  },
});

export default AppStack;
